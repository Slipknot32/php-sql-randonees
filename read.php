<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Randonnées</title>
    <link rel="stylesheet" href="css/basics.css" media="screen" title="no title" charset="utf-8">
  </head>
  <body>
    <h1>Liste des randonnées</h1>

    <?php

    include 'morceaux/connect.php';
    include 'morceaux/inc.php';  ///////////////////// création 5 première rando

    //foreach($bdd->query('SELECT * FROM hiking') as $rando) {
    //echo $rando[name].' '.$rando[difficulty].' '.$rando[distance].' '.$rando[duration].' '.$rando[height_difference]. '<br><br>';
    //}
    ?>

    <table>
      <thead>
        <tr>
          <td>name</td>
          <td>difficulty</td>
          <td>distance</td>
          <td>duration</td>
          <td>height_difference</td>
        </tr>
      </thead>

      <tbody>

        <?php
        $results = $bdd->query("SELECT * FROM hiking ");
        while ($row= $results->fetch()){
        ?>
        
        <tr>
          <td><?= $row['name']?></td>
          <td><?= $row['difficulty']?></td>
          <td><?= $row['distance']?></td>
          <td><?= $row['duration']?></td>
          <td><?= $row['height_difference']?></td>
        </tr>

        <?php
        }
        ?>
        
      </tbody>
    </table>
  </body>
</html>